package com.rathravane.drift.builder.sources;

import java.io.InputStream;
import java.io.Reader;

import org.json.JSONObject;

import com.rathravane.drift.builder.BuilderDataSource;
import com.rathravane.drift.builder.common.CommonDataSource;
import com.rathravane.till.data.json.CommentedJsonTokener;

/**
 * A builder data source backed by the json.org JSON object implementation.
 * @author peter
 *
 */
public class BuilderJsonDataSource extends CommonDataSource implements BuilderDataSource
{
	public BuilderJsonDataSource ( JSONObject data )
	{
		super ( JSONObject.class, "fromJson", data );
		fData = data;
	}

	public BuilderJsonDataSource ( InputStream data )
	{
		this ( new JSONObject ( new CommentedJsonTokener ( data ) ) );
	}

	public BuilderJsonDataSource ( Reader data )
	{
		this ( new JSONObject ( new CommentedJsonTokener ( data ) ) );
	}

	public BuilderJsonDataSource ( String data )
	{
		this ( new JSONObject ( new CommentedJsonTokener ( data ) ) );
	}

	@Override
	public String getClassNameFromData ()
	{
		// the service code always used "classname" but the builder code used
		// "class."  For compatibility, we'll allow both but prefer "class".

		String cn = fData.optString ( "class", null );
		if ( cn == null )
		{
			cn = fData.optString ( "classname", null );
		}
		return cn;
	}

	private final JSONObject fData;
}
