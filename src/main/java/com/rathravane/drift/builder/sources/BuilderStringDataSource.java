package com.rathravane.drift.builder.sources;

import com.rathravane.drift.builder.Builder.BuildFailure;
import com.rathravane.drift.builder.BuilderDataSource;
import com.rathravane.drift.builder.common.CommonDataSource;

/**
 * A builder data source backed by the json.org JSON object implementation.
 * @author peter
 *
 */
public class BuilderStringDataSource extends CommonDataSource implements BuilderDataSource
{
	public BuilderStringDataSource ( String data )
	{
		super ( String.class, "fromString", data );
	}

	@Override
	public String getClassNameFromData () throws BuildFailure 
	{
		throw new BuildFailure ( "String data source doesn't contain a classname." );
	}
}
