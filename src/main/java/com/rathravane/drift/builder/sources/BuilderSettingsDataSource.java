package com.rathravane.drift.builder.sources;

import com.rathravane.drift.builder.BuilderDataSource;
import com.rathravane.drift.builder.common.CommonDataSource;
import com.rathravane.till.nv.rrNvReadable;

/**
 * A builder data source backed by a Rathravane settings instance.
 * @author peter
 *
 */
public class BuilderSettingsDataSource extends CommonDataSource implements BuilderDataSource
{
	public BuilderSettingsDataSource ( rrNvReadable data )
	{
		super ( rrNvReadable.class, "fromSettings", data );
		fData = data;
	}

	@Override
	public String getClassNameFromData ()
	{
		return fData.getString ( "class", null );
	}

	private final rrNvReadable fData;
}
