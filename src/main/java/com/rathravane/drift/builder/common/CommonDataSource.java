package com.rathravane.drift.builder.common;

import com.rathravane.drift.builder.BuilderDataSource;

public abstract class CommonDataSource implements BuilderDataSource
{
	protected CommonDataSource ( Class<?> initClass, String initMethod, Object data )
	{
		fInitClass = initClass;
		fInitMethod = initMethod;
		fData = data;
	}

	@Override
	public Class<?> getIniterClass () { return fInitClass; }

	@Override
	public String getIniterName () { return fInitMethod; }

	@Override
	public Object getInitData () { return fData; }

	private final Class<?> fInitClass;
	private final String fInitMethod;
	private final Object fData;
}
