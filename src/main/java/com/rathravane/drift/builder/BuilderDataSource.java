package com.rathravane.drift.builder;

import com.rathravane.drift.builder.Builder.BuildFailure;

public interface BuilderDataSource
{
	/**
	 * Get the string value for the classname
	 * @return the classname or null
	 */
	String getClassNameFromData () throws BuildFailure;

	/**
	 * Get the class used in the object constructor or static initializer call
	 * @return the class to use in initialization
	 */
	Class<?> getIniterClass ();

	/**
	 * Get the name of the static initializer method on the target class
	 * @return
	 */
	String getIniterName ();

	/**
	 * Get the actual data for the init function (constructor, static fromX(), etc.)
	 * @return a data object
	 */
	Object getInitData ();
}
