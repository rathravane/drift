package com.rathravane.drift.resources.sources;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.rathravane.drift.resources.ResourceLoader;
import com.rathravane.drift.resources.ResourceSource;
import com.rathravane.till.data.rrStreamTools;

public class AwsS3BucketAndKeyLoader implements ResourceSource
{
	public AwsS3BucketAndKeyLoader ( String bucket )
	{
		fClient = null;
		fBucket = bucket;

		fCreds = new AWSCredentials ()
		{
			@Override
			public String getAWSAccessKeyId () { return System.getenv ( "AWS_ACCESS_KEY_ID" ); }

			@Override
			public String getAWSSecretKey () { return System.getenv ( "AWS_SECRET_ACCESS_KEY" ); }
		};
	}

	@Override
	public boolean qualifies ( String resourceId )
	{
		return true;
	}

	@Override
	public InputStream loadResource ( String resourceId ) throws IOException
	{
		try
		{
			if ( fClient == null )
			{
				fClient = AmazonS3ClientBuilder
					.standard ()
					.withCredentials ( new AWSStaticCredentialsProvider ( fCreds ) )
					.build ()
				;
			}

			final ByteArrayOutputStream baos = new ByteArrayOutputStream ();
			S3Object object = null;
			try
			{
				object = fClient.getObject ( new GetObjectRequest ( fBucket, resourceId ) );
				final InputStream is = object.getObjectContent ();
	
				// s3 objects must be closed or will leak an HTTP connection
				rrStreamTools.copyStream ( is, baos );
			}
			catch ( AmazonS3Exception x )
			{
				throw new IOException ( x ); 
			}
			finally
			{
				if ( object != null )
				{
					object.close ();
				}
			}
			return new ByteArrayInputStream ( baos.toByteArray () );
		}
		catch ( java.lang.NoClassDefFoundError x )
		{
			log.warn ( "URL [" + resourceId + "] requires the AWS S3 libraries but they're not available. " + x.getMessage (), x );
			throw new IOException ( x );
		}
	}

	private final AWSCredentials fCreds;
	private final String fBucket;
	private AmazonS3 fClient;

	private static final Logger log = LoggerFactory.getLogger ( ResourceLoader.class );
}
