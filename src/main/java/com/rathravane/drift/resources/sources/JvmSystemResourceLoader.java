package com.rathravane.drift.resources.sources;

import java.io.IOException;
import java.io.InputStream;

import com.rathravane.drift.resources.ResourceSource;

public class JvmSystemResourceLoader implements ResourceSource
{
	@Override
	public boolean qualifies ( String resourceId )
	{
		/* almost any name works here */
		return true;
	}

	@Override
	public InputStream loadResource ( String resourceId ) throws IOException
	{
		return ClassLoader.getSystemResourceAsStream ( resourceId );
	}
}
