package com.rathravane.drift.resources.sources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.rathravane.drift.resources.ResourceSource;

public class HttpLoader implements ResourceSource
{
	@Override
	public boolean qualifies ( String resourceId )
	{
		return ( resourceId.startsWith ( "http://" ) || resourceId.startsWith ( "https://" ) );
	}

	@Override
	public InputStream loadResource ( String resourceId ) throws IOException
	{
		return new URL ( resourceId ).openStream ();
	}
}
