package com.rathravane.drift.resources.sources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.drift.resources.ResourceLoader;
import com.rathravane.drift.resources.ResourceSource;

public class FileLoader implements ResourceSource
{
	public FileLoader ()
	{
		this ( null );
	}

	public FileLoader ( File base )
	{
		fBase = base;
	}

	@Override
	public boolean qualifies ( String resourceId )
	{
		// pretty much anything *might* be a filename
		return true;
	}

	@Override
	public InputStream loadResource ( String resourceId ) throws IOException
	{
		// try as a file
		final File f = fBase == null ?
			new File ( resourceId ) :
			new File ( fBase, resourceId )
		;
		if ( f.exists () )
		{
			try
			{
				return new FileInputStream ( resourceId );
			}
			catch ( FileNotFoundException x )
			{
				log.warn ( "File not found after exists() returned true.", x );
			}
		}
		return null;
	}

	private final File fBase;

	private static final Logger log = LoggerFactory.getLogger ( ResourceLoader.class );
}
