package com.rathravane.drift.resources.sources;

import java.io.IOException;
import java.io.InputStream;

import com.rathravane.drift.resources.ResourceSource;

public class JvmClassLoaderResourceLoader implements ResourceSource
{
	public JvmClassLoaderResourceLoader ( Class<?> startingPoint )
	{
		fClazz = startingPoint;
	}

	@Override
	public boolean qualifies ( String resourceId )
	{
		/* almost any name works here */
		return true;
	}

	@Override
	public InputStream loadResource ( String resourceId ) throws IOException
	{
		return fClazz.getClassLoader().getResourceAsStream ( resourceId );
	}

	private final Class<?> fClazz;
}
