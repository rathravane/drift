package com.rathravane.drift.resources;

import java.io.IOException;
import java.io.InputStream;

public interface ResourceSource
{
	/**
	 * Does the resource ID possibly qualify as served by this source?
	 * @param resourceId
	 * @return true if the resource ID might be handled by this source
	 */
	boolean qualifies ( String resourceId );

	/**
	 * Attempt to load the named resource.
	 * @param resourceId
	 * @return the resource as a stream, or null if it does not exist in this source
	 * @throws IOException 
	 */
	InputStream loadResource ( String resourceId ) throws IOException;
}
