package com.rathravane.drift.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import com.rathravane.drift.resources.sources.AwsS3UriLoader;
import com.rathravane.drift.resources.sources.FileLoader;
import com.rathravane.drift.resources.sources.HttpLoader;
import com.rathravane.drift.resources.sources.JvmClassLoaderResourceLoader;
import com.rathravane.drift.resources.sources.JvmSystemResourceLoader;
import com.rathravane.till.data.rrConvertor;

/**
 * ResourceLoader is a general purpose tool for loading a data stream by name.
 * @author peter
 */
public class ResourceLoader
{
	/**
	 * Attempt to load the named resource using our standard sources.
	 * @param resource
	 * @return the resource input stream or null
	 * @throws IOException
	 */
	public static InputStream load ( String resource ) throws IOException
	{
		return new ResourceLoader().named ( resource ).load ();
	}

	/**
	 * Install standard sources, which are, in order:<br>
	 * <br>
	 * S3 URI (when available in the CLASSPATH)<br>
	 * HTTP URL<br>
	 * Filesystem name<br>
	 * JVM classloader with this class as the base path<br>
	 * JVM system classloader<br>
	 * <br>
	 * 
	 * @return this
	 */
	public ResourceLoader usingStandardSources ()
	{
		return usingStandardSources ( true );
	}

	/**
	 * Install standard sources, with control over using networked resources.<br>
	 * 
	 * @return this
	 */
	public ResourceLoader usingStandardSources ( boolean withNetwork )
	{
		if ( withNetwork )
		{
			// we'll use S3 if the libraries are available
			if ( s3Available () )
			{
				usingSource ( new AwsS3UriLoader () );
			}
			usingSource ( new HttpLoader () );
		}

		return this
			.usingSource ( new FileLoader () )
			.usingSource ( new JvmClassLoaderResourceLoader ( ResourceLoader.class ) )	// not that useful!
			.usingSource ( new JvmSystemResourceLoader () )
		;
	}

	/**
	 * Add a resource source. You can call this repeatedly to add a number of sources
	 * and they'll be searched in the order provided.
	 * @param rs
	 * @return this
	 */
	public ResourceLoader usingSource ( ResourceSource rs )
	{
		fSrcSpecd = true;
		fSources.add ( rs );
		return this;
	}

	/**
	 * Establish the name of the resource being sought
	 * @param name
	 * @return this
	 */
	public ResourceLoader named ( String name )
	{
		fNamed = name;
		return this;
	}

	/**
	 * Load the resource. Caller must close the input stream. If no sources were explicitly
	 * added to this resource loader, the standard set is used (see usingStandardSources).
	 * This call searches each source, in the order provided, for the named item, returning
	 * the first found, if any. If not found in any source, null is returned.
	 * @return an input stream if the resource is found
	 * @throws IOException
	 */
	public InputStream load () throws IOException 
	{
		if ( fNamed == null ) return null;

		// if the user didn't say, assume standard sources
		if ( !fSrcSpecd ) usingStandardSources ();

		for ( ResourceSource src : fSources )
		{
			if ( src.qualifies ( fNamed ) )
			{
				final InputStream result = src.loadResource ( fNamed );
				if ( result != null )
				{
					return result;
				}
			}
		}

		return null;
	}

	@Override
	public String toString ()
	{
		return fNamed;
	}
	
	private LinkedList<ResourceSource> fSources = new LinkedList<ResourceSource> ();
	private String fNamed = null;
	private boolean fSrcSpecd = false;

	private static boolean sCheckedS3 = false;
	private static boolean sS3Available = false;
	private static boolean sS3Disabled = !rrConvertor.convertToBooleanBroad ( System.getenv ( "DRIFT_ALLOW_S3" ) );

	/**
	 * Check (first time only) if the Amazon S3 interface is available.
	 * @return true or false
	 */
	public static boolean s3Available ()
	{
		if ( !sCheckedS3 && !sS3Disabled )
		{
			sCheckedS3 = true;
			try
			{
				final Class<?> clazz = com.amazonaws.services.s3.AmazonS3.class;
				sS3Available = clazz != null;
			}
			catch ( NoClassDefFoundError x )
			{
				sS3Available = false;
			}
		}
		return sS3Available;
	}
}

