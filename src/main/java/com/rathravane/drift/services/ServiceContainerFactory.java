package com.rathravane.drift.services;

public interface ServiceContainerFactory<T extends ServiceContainer>
{
	T create ();
}
