package com.rathravane.drift.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.till.console.CmdLineParser;
import com.rathravane.till.console.CmdLinePrefs;
import com.rathravane.till.console.DaemonConsole;
import com.rathravane.till.nv.rrNvReadable;
import com.rathravane.till.nv.rrNvReadable.InvalidSettingValueException;
import com.rathravane.till.nv.rrNvReadable.MissingReqdSettingException;
import com.rathravane.till.nv.rrNvWriteable;

public class Server<T extends ServiceContainer> extends DaemonConsole
{
	public static final String kServices = "services";
	public static final String kServicesChar = "s";

	public static final String kProfile = "profile";
	public static final String kProfileChar = "p";

	protected Server ( String programName, ServiceContainerFactory<T> factory )
	{
		super ( programName );

		fFactory = factory;
		fServices = null;
	}

	@Override
	protected Server<T> setupDefaults ( rrNvWriteable pt )
	{
		pt.set ( kServices, "services.json" );
		return this;
	}

	@Override
	protected Server<T> setupOptions ( CmdLineParser p )
	{
		super.setupOptions ( p );

		p.registerOptionWithValue ( kServices, kServicesChar, null, null );
		p.registerOptionWithValue ( kProfile, kProfileChar, null, null );

		return this;
	}

	@Override
	protected Looper init ( rrNvReadable p, CmdLinePrefs clp ) throws MissingReqdSettingException, InvalidSettingValueException, StartupFailureException 
	{
		super.quietStartup ();
		for ( String line : kGreeting1 )
		{
			log.info ( line.replaceAll ( "PROGNAME", getProgramName () ) );
		}
		for ( String notice : getCopyrightLines () )
		{
			log.info ( notice );
		}
		for ( String line : kGreeting2 )
		{
			log.info ( line.replaceAll ( "PROGNAME", getProgramName () ) );
		}

		final Looper result = super.init ( p, clp );

		final String services = p.get ( kServices );
		if ( services == null )
		{
			log.warn ( "No services configuration name provided." );
		}
		else
		{
			log.info ( "Loading services from [" + services + "]..." );

			final URL url = ClassLoader.getSystemResource ( services );
			if ( url != null )
			{
				try
				{
					final InputStream serviceStream = url.openStream ();
					if ( serviceStream != null )
					{
						fServices = ServiceContainer.build (
							serviceStream,
							p.getStrings ( kProfile, new String[]{"default"} ),
							true,
							fFactory
						);
					}
				}
				catch ( IOException e )
				{
					log.warn ( "Couldn't open " + services + "(" + url.toString () + ") as stream." );
				}
			}
			else
			{
				log.warn ( "Couldn't find resource " + services + "." );
			}

			if ( fServices == null )
			{
				log.warn ( "No services loaded from " + services );
			}
		}
		
		return result;
	}

	@Override
	protected boolean daemonStillRunning ()
	{
		if ( fServices != null )
		{
			for ( Service sc : fServices.getServices () )
			{
				if ( sc.isRunning () ) return true;
			}
		}
		log.info ( "No services running." );
		return false;
	}

	public static void runServer ( String programName, String args[] ) throws Exception
	{
		runServer ( programName, new StdFactory (), args );
	}

	public static <T extends ServiceContainer> void runServer ( String programName, ServiceContainerFactory<T> factory, String args[] ) throws Exception
	{
		new Server<> ( programName, factory )
			.runFromMain ( args )
		;
	}

	private final ServiceContainerFactory<T> fFactory;
	private ServiceContainer fServices;

	private static final Logger log = LoggerFactory.getLogger ( Server.class );

	private static final String[] kGreeting1 =
	{
		". . . . . . . . . . . . . . . . . . . . . . . .",
		"PROGNAME (Drift Server)",
		"Created by Rathravane; Fairport NY USA",
		"Licensed under the Apache License, Version 2.0",
	};
	private static final String[] kGreeting2 =
	{
		". . . . . . . . . . . . . . . . . . . . . . . .",
	};

	public static class StdFactory implements ServiceContainerFactory<ServiceContainer>
	{
		@Override
		public ServiceContainer create ()
		{
			return new ServiceContainer ();
		}
	}
}
