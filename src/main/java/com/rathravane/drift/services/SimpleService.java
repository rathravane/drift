package com.rathravane.drift.services;

import com.rathravane.drift.services.Service;

/**
 * A simple service doesn't start/stop.
 * @author peter
 */
public class SimpleService implements Service
{
	@Override
	public synchronized void start () throws FailedToStart
	{
		fStopped = false;
		onStartRequested ();
	}

	@Override
	public synchronized void requestFinish ()
	{
		fStopped = true;
		onStopRequested ();
	}

	@Override
	public synchronized boolean isRunning ()
	{
		return !fStopped;
	}

	private boolean fStopped = true;

	protected void onStartRequested () throws FailedToStart {}
	protected void onStopRequested () {}
}
