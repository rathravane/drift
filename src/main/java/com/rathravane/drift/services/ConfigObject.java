package com.rathravane.drift.services;

import java.util.Collection;
import java.util.TreeSet;

import org.json.JSONObject;

import com.rathravane.till.data.json.JsonUtil;

public class ConfigObject
{
	public ConfigObject ()
	{
		this ( new JSONObject () );
	}

	public ConfigObject ( JSONObject o )
	{
		fBase = null;
		fData = JsonUtil.clone ( o );
	}

	public static ConfigObject read ( JSONObject o )
	{
		return new ConfigObject ( o );
	}

	@Override
	public String toString ()
	{
		return toJson().toString ( 4 );
	}

	public JSONObject toJson ()
	{
		JSONObject merged = fBase == null ? new JSONObject () : fBase.toJson ();
		JsonUtil.copyInto ( fData, merged );
		return merged;
	}

	public int size ()
	{
		return fData.length ();
	}

	public ConfigObject setBaseConfig ( ConfigObject co )
	{
		fBase = co;
		return this;
	}
	
	public String get ( String key )
	{
		return get ( key, null );
	}

	public String get ( String key, String defval )
	{
		if ( fData.has ( key ) )
		{
			return fData.getString ( key );
		}
		else if ( fBase != null )
		{
			return fBase.get ( key, defval );
		}
		return defval;
	}

	public boolean getBoolean ( String key )
	{
		return getBoolean ( key, false );
	}

	public boolean getBoolean ( String key, boolean defval )
	{
		if ( fData.has ( key ) )
		{
			return fData.optBoolean ( key );
		}
		else if ( fBase != null )
		{
			return fBase.getBoolean ( key, defval );
		}
		return defval;
	}

	public Collection<String> getAllKeys ()
	{
		final TreeSet<String> keys = new TreeSet<> ();
		if ( fBase != null )
		{
			keys.addAll ( fBase.getAllKeys () );
		}
		for ( Object key : fData.keySet () )
		{
			keys.add ( key.toString () );
		}
		return keys;
	}

	private final JSONObject fData;
	private ConfigObject fBase;

	protected void set ( String key, String value )
	{
		fData.put ( key, value );
	}

	protected ConfigObject getSubConfig ( String serviceName )
	{
		final JSONObject data = fData.optJSONObject ( serviceName );
		return data == null ? new ConfigObject () : new ConfigObject ( data );
	}
}
