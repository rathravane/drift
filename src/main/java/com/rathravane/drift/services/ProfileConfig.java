package com.rathravane.drift.services;

import org.json.JSONObject;

public class ProfileConfig extends ConfigObject
{
	public ProfileConfig ( JSONObject p )
	{
		super ( p );
	}

	public static ProfileConfig read ( JSONObject p )
	{
		return new ProfileConfig ( p );
	}

	public ConfigObject getConfigOverridesFor ( String serviceName )
	{
		return super.getSubConfig ( serviceName );
	}
}
