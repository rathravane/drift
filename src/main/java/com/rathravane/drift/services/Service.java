package com.rathravane.drift.services;

public interface Service
{
	public class FailedToStart extends Exception
	{
		public FailedToStart ( Throwable t ) { super(t); }
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * Start this service, post construction.
	 */
	void start () throws FailedToStart;

	/**
	 * Request this service to shutdown.
	 */
	void requestFinish ();

	/**
	 * Return true if the service is running.
	 * @return true if the service is running
	 */
	boolean isRunning ();
}
